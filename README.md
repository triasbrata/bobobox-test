
# Back-End Assessment
 ## Database Schema & Flowchart
 - [database schema](https://dbdiagram.io/d/5fad17303a78976d7b7b9bc5)
 - [flowchart](./how-to/flowchats/index.md)
 ## Promotion Service
 Applikasi ini di build dengan menggunakan framework [Nest.js](https://nestjs.com) dengan Typescript sebagai bahasa pemogramannya.

 ### Install Depedency
 ```bash
 #if use yarn 
 $ yarn 

 #if use npm
 $ npm i
 ```
 
 ### Build App
 ```bash
 #if use yarn 
 $ yarn build 

 #if use npm
 $ npm run build
 ```
 
 ### Run App
 ```bash
 #if use yarn 
 $ yarn start 

 #if use npm
 $ npm run start
 ```
### Project Structure
```bash
src # root folder
├── app.controller.ts # controller
├── app.module.ts # module
├── app.service.ts #service 
├── helpers
│   ├── Day.ts #day constanta
│   ├── Mybe.ts # type mybe null or else
│   └── constant.ts # general constanta
├── main.ts #main file
└── types
    ├── BookingDto.ts #booking dto
    └── Promotion.ts # promotion interface 
```

### Testing

silahkan import file `bobobox.postman_collection.json` ke postman. di collection yang baru di import terdapat 3 request, dimana
- **calculatePromo** merupakan end point untuk melakukan perhitungan promo. untuk sintaksnya bisa di cek di file `src/app.controller.ts` pada method `handleCalculatePromotion`
- **listPromo** merupakan end point untuk melihat list promo yang ada. sumber data di dapatkan dari file `promotion.json` dan dapat dimanipulasi sesuai dengan kebutuhan. untuk dapat melihat sintaksnya dapat di cek di file controller pada method `handleListPromotions`
- **listPromousage** merupakan end point untuk melihat jumlah penggunaan promosi serta distribusinya pada tanggal berapa dan jumlah pemakaian pada hari tersebut. data ini akan ter-reset setiap applikasi di hidupkan. untuk mengecek sintaksnya dapat di cek di file controller pada method `handleListUsagePromotion`

