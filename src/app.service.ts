import { Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { readFileSync } from 'fs';
import { sumBy } from 'lodash';
import { resolve } from 'path';
import { Promotion } from './types/Promotion';

export interface DetailPromotionUsage {
  date: string;
  num: number;
}
export interface PromotionUsage {
  id: number;
  usage: DetailPromotionUsage[];
}

@Injectable()
export class AppService implements OnApplicationBootstrap {
  private _promotionUsage!: PromotionUsage[];
  onApplicationBootstrap() {
    this._promotionUsage = this.getListPromotions().map(it => {
      return {
        id: it.id,
        usage: [],
      };
    });
  }
  addPromotion(day: string, id: number) {
    this._promotionUsage = this._promotionUsage.map(it => {
      const usage = it.usage;
      if (it.id === id) {
        const index = usage.findIndex(it => it.date === day);
        if (index == -1) {
          usage.push({
            date: day,
            num: 1,
          });
        } else {
          usage[index].num += 1;
        }
      }
      return {
        ...it,
        usage,
      };
    });
  }
  isQuotaAvaliable(promotion: Promotion): [boolean, PromotionUsage] {
    const avaliableQuota = this.getPromotionQuotaUsagePerDay().find(
      it => it.id == promotion.id,
    );

    if (!avaliableQuota) {
      return [false, null];
    }
    const totalUsage = sumBy(avaliableQuota.usage, it => it.num);
    if (promotion.quota < totalUsage) {
      return [false, null];
    }

    return [true, avaliableQuota];
  }
  isQoutaDayAvalible(date: string, promotion: Promotion) {
    const [isAvalible, avaliableQuota] = this.isQuotaAvaliable(promotion);
    if (!isAvalible) {
      return false;
    }
    const quotaUsagePerDay = avaliableQuota.usage.find(it => it.date === date);
    if (!quotaUsagePerDay) {
      return true;
    }
    if (promotion.quotaInDay < quotaUsagePerDay.num) return false;
    return true;
  }

  getPromotion(promo_id: number) {
    return this.getListPromotions().find(
      it => Number(it.id) === Number(promo_id),
    );
  }
  getListPromotions() {
    const promotions: Promotion[] = JSON.parse(
      readFileSync(resolve('promotion.json')).toString(),
    );
    return promotions;
  }
  getPromotionQuotaUsagePerDay() {
    return this._promotionUsage;
  }
}
