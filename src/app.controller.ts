import { Body, Controller, Get, Post } from '@nestjs/common';
import * as moment from 'moment';
import { AppService } from './app.service';
import { DATE_FORMAT, DAY_FORMAT, TIME_FORMAT } from './helpers/constant';
import { BookingDto } from './types/BookingDto';
import { discType } from './types/Promotion';
import { sumBy, sum } from 'lodash';
import { extendMoment } from 'moment-range';
extendMoment(moment);
interface ResponseCalculate {
  listDetail: { room_id: number; promoPrice: number }[];
  promoPrice: number;
  finalPrice: number;
}

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  handleListPromotions() {
    return this.appService.getListPromotions();
  }
  @Get('usage')
  handleListUsagePromotion() {
    return this.appService.getPromotionQuotaUsagePerDay();
  }
  @Post()
  handleCalculatePromotion(@Body() input: BookingDto): ResponseCalculate {
    const now = moment();
    const promotion = this.appService.getPromotion(input.promo_id);
    const baseResponse = {
      listDetail: input.listItem.map(it => ({
        room_id: it.room_id,
        promoPrice: 0,
      })),
      promoPrice: 0,
      finalPrice: input.totalPrice,
    };

    if (!promotion) {
      return baseResponse;
    }
    const [isQuotaAvaliable] = this.appService.isQuotaAvaliable(promotion);
    if (!isQuotaAvaliable) {
      return baseResponse;
    }
    if (promotion.rules.bookingDay !== now.format(DAY_FORMAT).toLowerCase()) {
      return baseResponse;
    }

    let findTime = false;
    const checkHours = promotion.rules.bookingHours.map(it => {
      if (findTime) {
        return false;
      }
      const start = moment(it.start, TIME_FORMAT);
      const end = moment(it.end, TIME_FORMAT);
      const isInRage = now.diff(start) > 0 && now.diff(end) < 0;
      if (isInRage) {
        findTime = true;
      }
      return true;
    });
    if (!checkHours.includes(true)) {
      return baseResponse;
    }
    const passMinRoom = promotion.rules.minRoomUse < input.listItem.length;
    if (!passMinRoom) {
      return baseResponse;
    }
    const listItemWithPromotion = input.listItem.map(it => {
      const baseIt = { room_id: it.room_id, promoPrice: 0 };

      const mDateStart = moment(it.dateStart);
      const mDateEnd = moment(it.dateEnd);
      const passMinNight =
        mDateEnd.diff(mDateStart, 'days') > promotion.rules.minNights;
      if (!passMinNight) {
        return baseIt;
      }

      const startEnd = (moment() as any).range(mDateStart, mDateEnd);
      const range = Array.from(
        startEnd.by('days', { excludeEnd: true }),
      ) as moment.Moment[];
      if (
        !range
          .map(it => it.format(DAY_FORMAT).toLowerCase())
          .includes(promotion.rules.checkinDay)
      ) {
        return baseIt;
      }
      const prom = [];

      for (const date of range) {
        const day = date.format(DATE_FORMAT);
        const avaliableQuota = this.appService.isQoutaDayAvalible(
          day,
          promotion,
        );
        if (avaliableQuota) {
          this.appService.addPromotion(day, Number(promotion.id));
          prom.push(
            this.calculatePromotion(
              promotion.discountType,
              promotion.discountAmount,
              it.price,
            ),
          );
        }
      }
      return {
        room_id: it.room_id,
        promoPrice: sum(prom),
      };
    });
    const promoPrice = sumBy(listItemWithPromotion, it => it.promoPrice);
    return {
      listDetail: listItemWithPromotion,
      promoPrice: promoPrice,
      finalPrice: baseResponse.finalPrice - promoPrice,
    };
  }
  calculatePromotion(
    discountType: discType,
    discountAmount: number,
    roomPrice: number,
  ): any {
    if (discountType === 'currency') {
      return discountAmount;
    }
    return (discountAmount * roomPrice) / 100;
  }
}
