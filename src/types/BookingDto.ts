export interface BookDetail {
  dateStart: Date;
  dateEnd: Date;
  numberRoom: number;
  price: number;
  room_id: number;
}
export interface BookingDto {
  promo_id: number;
  totalPrice: number;
  listItem: BookDetail[];
}
