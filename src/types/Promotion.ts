export type discType = 'percentage' | 'currency';

export interface Promotion {
  id: number;
  quota: number;
  discountType: discType;
  discountAmount: number;
  quotaInDay: number;
  rules: Rules;
}

export interface Rules {
  minRoomUse: number;
  minNights: number;
  checkinDay: string;
  bookingDay: string;
  bookingHours: BookHour[];
}

export interface BookHour {
  start: string;
  end: string;
}
