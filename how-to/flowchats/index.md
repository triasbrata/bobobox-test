
# Flowcharts
open in [draw.io](https://viewer.diagrams.net/?highlight=0000ff&edit=_blank&layers=1&nav=1&title=Untitled%20Diagram.drawio#Uhttps%3A%2F%2Fdrive.google.com%2Fuc%3Fid%3D1NaLhjVoMs73-mpLNrVYTdl4XvNAXNtaj%26export%3Ddownload)

![flow chart hotels management](./Untitled%20Diagram-flow%20chart%20management%20hotel.jpg)
![flow chart CRUD hotel](./Untitled%20Diagram-manage%20hotels.jpg)